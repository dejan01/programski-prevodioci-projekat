
main:
		PUSH	%14
		MOV 	%15,%14
		MOV 	$11,-12(%14)
		MOV 	$2,-16(%14)
		MOV 	$3,-20(%14)
		MOV 	$4,-24(%14)
		MOV 	$11,-28(%14)
		MOV 	$2,-32(%14)
		MOV 	$3,-36(%14)
		MOV 	$4,-40(%14)
		MOV 	$11,-44(%14)
		MOV 	$2,-48(%14)
		MOV 	$3,-52(%14)
		MOV 	$4,-56(%14)
		MOV 	$11,-60(%14)
		MOV 	$2,-64(%14)
		MOV 	$3,-68(%14)
		MOV 	$4,-72(%14)
		SUBS	%15,$72,%15
@main_body:
		MOV 	$16,-4(%14)
		MOV 	-4(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET