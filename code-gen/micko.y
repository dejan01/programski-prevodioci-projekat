%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include "defs.h"
  #include "symtab.h"
  #include "codegen.h"

  int yyparse(void);
  int yylex(void);
  int yyerror(char *s);
  void warning(char *s);

  extern int yylineno;
  int out_lin = 0;
  char char_buffer[CHAR_BUFFER_LENGTH];
  int error_count = 0;
  int warning_count = 0;
  int var_num = 0;
  int fun_idx = -1;
  int fcall_idx = -1;
  int lab_num = -1;
  int array_init = 0;
  int part_of_array = 0;
  int array_types[7];
  int regular_types_to_array[7];
  int array_el_num = 0;
  int id_of_array_for_init = 0;
  int first_in_assignment = 0;
  int matrix_current_row = 0;
  int assignment_matrix_id = 0;
  FILE *output;

%}

%union {
  int i;
  struct Exp_return *test;
  int assign_array [1000];
  char *s;
}

%token <i> _TYPE
%token _IF
%token _ELSE
%token _RETURN
%token <s> _ID
%token <s> _INT_NUMBER
%token <s> _UINT_NUMBER
%token _LPAREN
%token _RPAREN
%token _LBRACKET
%token _RBRACKET
%token _LSQBRACKET
%token _RSQBRACKET
%token _ASSIGN
%token _COMMA
%token _SEMICOLON
%token _COLON
%token _LENGTH
%token <i> _AROP
%token <i> _RELOP

%type <test> exp
%type <assign_array> possible_array_assign
%type <i> num_exp  literal matrix_elements possible_init_matrix_elements
%type <i> function_call argument rel_exp if_part possible_init_array array_elements  

%nonassoc ONLY_IF
%nonassoc _ELSE

%%

program
    : { 
	array_types[1] = 1;
	array_types[2] = 2;
	array_types[3] = 1;
	array_types[4] = 2;
	array_types[5] = 1;
	array_types[6] = 2;
	regular_types_to_array[1] = 3;
	regular_types_to_array[2] = 4;
	}
function_list
      {  
	/*int x = (*$2).index;
	printf("%d",x);*/
        if(lookup_symbol("main", FUN) == NO_INDEX)
          err("undefined reference to 'main'");
      }
  ;

function_list
  : function	
/*{struct Exp_return vrati; vrati.index = 10; $$ = &vrati;}*/
  | function_list function
/*{struct Exp_return vrati; vrati.index = 10;$$ = &vrati;}*/
  ;

function
  : _TYPE _ID
      {
        fun_idx = lookup_symbol($2, FUN);
        if(fun_idx == NO_INDEX)
          fun_idx = insert_symbol($2, FUN, $1, NO_ATR, NO_ATR, NO_ATR);
        else 
          err("redefinition of function '%s'", $2);

        code("\n%s:", $2);
        code("\n\t\tPUSH\t%%14");
        code("\n\t\tMOV \t%%15,%%14");
      }
    _LPAREN parameter _RPAREN body
      {
        clear_symbols(fun_idx + 1);
        var_num = 0;
        
        code("\n@%s_exit:", $2);
        code("\n\t\tMOV \t%%14,%%15");
        code("\n\t\tPOP \t%%14");
        code("\n\t\tRET");
      }
  ;

parameter
  : /* empty */
      { set_atr1(fun_idx, 0); }
  | _TYPE _ID
      {
	int atr3 = ($1 == INTMATRIX || $1 == UINTMATRIX) ? 1 : 0;
	int type = ($1 == INTMATRIX) ? INTARRAY : $1;
	type = ($1 == UINTMATRIX) ? UINTARRAY : type;
	printf("AAAA%d\n",type);
        insert_symbol($2, PAR, type, 1, NO_ATR, atr3);
        set_atr1(fun_idx, 1);
        set_atr2(fun_idx, type);
      }
  ;

body
  : _LBRACKET variable_list
      {
        if(var_num)
          code("\n\t\tSUBS\t%%15,$%d,%%15", 4*var_num);
        code("\n@%s_body:", get_name(fun_idx));
      }
    statement_list _RBRACKET
  ;

variable_list
  : /* empty */
  | variable_list variable
  ;

variable
  : _TYPE _ID _SEMICOLON
      {
        if(lookup_symbol($2, VAR|PAR) == NO_INDEX)
           insert_symbol($2, VAR, $1, ++var_num, NO_ATR, NO_ATR);
        else 
           err("redefinition of '%s'", $2);
      }
  | _TYPE _ID _LSQBRACKET _INT_NUMBER _RSQBRACKET
{
	int type = regular_types_to_array[$1];
        if(lookup_symbol($2, VAR|PAR) == NO_INDEX)
	   {
           	insert_symbol($2, VAR, type, ++var_num, atoi($4), NO_ATR);
		printf("\n%d=",var_num);
		var_num += atoi($4)-1;
		printf("%d\n",var_num);
           }
        else 
           err("redefinition of '%s'", $2);
	id_of_array_for_init  = lookup_symbol($2, VAR|PAR);
}
 possible_init_array _SEMICOLON
{
	if($7!=-1 && atoi($4) != $7){
	   err("init doesnt have same number of elements as array %s",$2);	
	}
	id_of_array_for_init = 0;
        matrix_current_row = 0;
}
  | _TYPE _ID _LSQBRACKET _INT_NUMBER _COMMA _INT_NUMBER _RSQBRACKET
{
	int type = regular_types_to_array[$1];
        if(lookup_symbol($2, VAR|PAR) == NO_INDEX)
	   {
		printf("\n%d=",var_num);
           	insert_symbol($2, VAR, type, ++var_num, atoi($4), atoi($6));
		printf("%d=",var_num);
		var_num += atoi($4)*atoi($6)-1;
		printf("%d\n",var_num);
           }
        else 
           err("redefinition of '%s'", $2);
	id_of_array_for_init  = lookup_symbol($2, VAR|PAR);
        matrix_current_row = 0;
}
 possible_init_matrix _SEMICOLON 
  ;

possible_init_matrix
  : _ASSIGN _LBRACKET possible_init_matrix_elements _RBRACKET 
{
    if($3 != get_atr2(id_of_array_for_init)) 
      err("row number not good");
}
  |
  ;

possible_init_matrix_elements
  : _LBRACKET matrix_elements _RBRACKET
{ if($2 != get_atr3(id_of_array_for_init)) 
    err("column number not good");
  $$ = 1;
  matrix_current_row++;
}
  | possible_init_matrix_elements _COMMA _LBRACKET matrix_elements _RBRACKET
{ if($4 != get_atr3(id_of_array_for_init)) 
    err("column number not good");
  $$++;
  matrix_current_row++;
}
  ;


matrix_elements
  : literal
{
	if(get_type($1) != array_types[get_type(id_of_array_for_init)]){
		err("element %s  in init is not the same type\n",get_name($1));
	}
	int index = matrix_current_row*get_atr3(id_of_array_for_init);
	printf("-----%d\n",index);
	gen_mov_add($1,id_of_array_for_init,0,index);
	$$=1;
}
  | matrix_elements _COMMA literal 
{
	if(get_type($3) != array_types[get_type(id_of_array_for_init)]){
		err("element %s in init is not the same type\n",get_name($3));
	}
	int index = matrix_current_row*get_atr3(id_of_array_for_init);
	gen_mov_add($3,id_of_array_for_init,0,index+$$);
	$$++;
}
  ;


possible_init_array
  : _ASSIGN _LBRACKET array_elements _RBRACKET {$$ = $3; }
  | { $$ = -1;}
  ;

array_elements
  : literal
{
	if(get_type($1) != array_types[get_type(id_of_array_for_init)]){
		err("element %s  in init is not the same type\n",get_name($1));
	}
	gen_mov_add($1,id_of_array_for_init,0,0);
	$$=1;
}
  | array_elements _COMMA literal 
{
	if(get_type($3) != array_types[get_type(id_of_array_for_init)]){
		err("element %s in init is not the same type\n",get_name($3));
	}
	gen_mov_add($3,id_of_array_for_init,0,$$);
	$$++;
}
  ;

//-------------------- initialization code done ----------------------

statement_list
  : /* empty */
  | statement_list statement
  ;

statement
  : compound_statement
  | assignment_statement
  | if_statement
  | return_statement
  ;

compound_statement
  : _LBRACKET statement_list _RBRACKET
  ;

assignment_statement
  : _ID {assignment_matrix_id = lookup_symbol($1,VAR|PAR);} possible_array_assign _ASSIGN num_exp _SEMICOLON
      {
        int idx = lookup_symbol($1, VAR|PAR);
	int idx_type = get_type(idx);
	int* idx_add = $3;
	if( idx_add[0] != -1){
	 idx_type = array_types[idx_type];
	}
	else{
	 idx_add[0] = 0;
	}
	int i = 0;
	while(idx_add[i] != -1){
		if(idx == NO_INDEX)
		  err("invalid lvalue '%s' in assignment", $1);
		else if(part_of_array == 0){
			  if(idx_type != get_type($5))
			    err("incompatible types in assignment");
			gen_mov_add($5, idx,0,idx_add[i]);
		}
		else {
		   int num_exp_type = get_type($5);
		   num_exp_type = array_types[num_exp_type];	
		  if(idx_type != num_exp_type)
		    err("incompatible types in assignment");
		  gen_mov_add($5, idx,array_el_num,idx_add[i]);
		}	
		i++;
	}
	first_in_assignment = 0;
	part_of_array = 0;
	assignment_matrix_id = 0;
      }
  ;

possible_array_assign
  : _LSQBRACKET _INT_NUMBER _RSQBRACKET { $$[0] = atoi($2); $$[1] = -1;
if((atoi($2) >= get_atr2(assignment_matrix_id) || atoi($2) < 0)){
  err("index %d out of the array %s",
atoi($2),get_name(assignment_matrix_id));	
if ((get_type(assignment_matrix_id)!=INTARRAY && get_type(assignment_matrix_id)!=UINTARRAY) || get_atr3(assignment_matrix_id) != 0){
	print_symtab();
	err("%s isn't an array, but it is being used like one",get_name(assignment_matrix_id));
}
}
 }

  | _LSQBRACKET _INT_NUMBER _COMMA _INT_NUMBER _RSQBRACKET 
{ 
if((atoi($2) >= get_atr2(assignment_matrix_id) || atoi($2) < 0) ||
    (atoi($4) >= get_atr3(assignment_matrix_id) || atoi($4) < 0)
){
  err("index [%d,%d] out of the matrix %s",atoi($2),atoi($4),get_name(assignment_matrix_id));	
}
if ((get_type(assignment_matrix_id)!=INTARRAY && get_type(assignment_matrix_id)!=UINTARRAY) || get_atr3(assignment_matrix_id) == 0){
	err("%s isn't an matrix, but it is being used like one",get_name(assignment_matrix_id));
}

$$[0] = atoi($2)*get_atr3(assignment_matrix_id) + atoi($4); 
$$[1] = -1;
}

  | _LSQBRACKET _COLON _COMMA _INT_NUMBER _RSQBRACKET 
{ 
	if((atoi($4) >= get_atr3(assignment_matrix_id) || atoi($4) < 0)){
	  err("index [:,%d] out of the matrix %s",atoi($4),get_name(assignment_matrix_id));	}
	if ((get_type(assignment_matrix_id)!=INTARRAY && get_type(assignment_matrix_id)!=UINTARRAY) || get_atr3(assignment_matrix_id) == 0){
	err("%s isn't an matrix, but it is being used like one",get_name(assignment_matrix_id));
}
int i = 0;
while(i < get_atr2(assignment_matrix_id)){
	$$[i] = i*get_atr3(assignment_matrix_id) + atoi($4); 
	i++;
}
$$[i] = -1;
}

  | _LSQBRACKET _INT_NUMBER _COMMA _COLON  _RSQBRACKET 
{ 
	if((atoi($2) >= get_atr2(assignment_matrix_id) || atoi($2) < 0)){
	  err("index [%d,:] out of the matrix %s",atoi($2),get_name(assignment_matrix_id));	}
	if ((get_type(assignment_matrix_id)!=INTARRAY && get_type(assignment_matrix_id)!=UINTARRAY) || get_atr3(assignment_matrix_id) == 0){
	err("%s isn't an matrix, but it is being used like one",get_name(assignment_matrix_id));
}
int i = 0;
while(i < get_atr3(assignment_matrix_id)){
	$$[i] = i + get_atr2(assignment_matrix_id)*atoi($2); 
	i++;
}
$$[i] = -1;
}
  


  | {$$[0]=-1;$$[1]=-1;}
  ;


//---------------------- assignment done ---------------------

num_exp
  : exp { $$ = (*$1).index; 
if(get_type((*$1).index) > 2){
	part_of_array = 1;
}
else{
	part_of_array = 0;
}
first_in_assignment = (*$1).el_num;
 }

  | num_exp _AROP exp
      {
	if(get_type($1) > 2){
		part_of_array = 1;
	}
	else{
		part_of_array = 0;
	}	
	int type1,type3;
	int exp_index = (*$3).index;
	int el_num = (*$3).el_num;
	type1 = get_type($1);
	type3 = get_type(exp_index);
	if(type1 == INTARRAY || type1 == UINTARRAY){
		type1 = array_types[type1];
	} 
	if(type3 == INTARRAY || type3 == UINTARRAY){
		type3 = array_types[type3];
	} 
        if(type1 != type3)
          err("invalid operands: arithmetic operation");
        int t1 = get_type($1);    
        code("\n\t\t%s\t", ar_instructions[$2 + (type1 - 1) * AROP_NUMBER]);
        gen_sym_name_add($1,first_in_assignment);
        code(",");
        gen_sym_name_add(exp_index,el_num);
        code(",");
        free_if_reg(exp_index);
        free_if_reg($1);
        $$ = take_reg();
        gen_sym_name($$);
        set_type($$, t1);
      }
  ;

exp
  : literal
{struct Exp_return vrati; vrati.index = $1; vrati.el_num=-1; $$ = &vrati;}
  | _ID
      {struct Exp_return vrati;
        vrati.index = lookup_symbol($1, VAR|PAR);
        if(vrati.index == NO_INDEX)
          err("'%s' undeclared", $1);
	vrati.el_num=0; $$ = &vrati;
      }

  | function_call
      {struct Exp_return vrati;
        vrati.index = take_reg();
        gen_mov(FUN_REG, vrati.index);
	vrati.el_num=0; $$ = &vrati;
      }
  
  | _LPAREN num_exp _RPAREN
      { struct Exp_return vrati;
        vrati.index = $2;
	vrati.el_num=0; $$ = &vrati; }
  | _ID _LSQBRACKET _INT_NUMBER _RSQBRACKET
      {
        int arr = lookup_symbol($1, VAR|PAR);
        if(arr == NO_INDEX)
          err("'%s' undeclared", $1);
	if((get_type(arr) != INTARRAY && get_type(arr) != UINTARRAY) || get_atr3(arr) != 0)
	  err("'%s' is not an array, but it is used like one", $1);
	if(atoi($3) >= get_atr2(lookup_symbol($1, VAR|PAR)) || atoi($3) < 0){
	  err("index %d out of the array",atoi($3));	
	}
	struct Exp_return vrati;
        vrati.index = lookup_symbol($1, VAR|PAR);
	vrati.el_num=atoi($3); $$ = &vrati;
	array_el_num = atoi($3);
      }
  | _ID _LSQBRACKET _INT_NUMBER _COMMA _INT_NUMBER _RSQBRACKET
      {
        int arr = lookup_symbol($1, VAR|PAR);
        if(arr == NO_INDEX)
          err("'%s' undeclared", $1);
	if ((get_type(arr)!=INTARRAY && get_type(arr)!=UINTARRAY) || get_atr3(arr) == 0){
	err("%s isn't an matrix, but it is being used like one",get_name(arr));
}
	if((atoi($3) >= get_atr2(arr) || atoi($3) < 0) ||
    (atoi($5) >= get_atr3(arr) || atoi($5) < 0)
){
  err("index [%d,%d] out of the matrix %s",atoi($3),atoi($5),get_name(arr));	
}
	struct Exp_return vrati;
        vrati.index = lookup_symbol($1, VAR|PAR);
	vrati.el_num=atoi($3)*get_atr3(lookup_symbol($1, VAR|PAR))+atoi($5); $$ = &vrati;
	array_el_num = atoi($3)*get_atr3(lookup_symbol($1, VAR|PAR))+atoi($5);
      }
  | _LENGTH _LPAREN _ID _RPAREN
{
char str[20];
int idx = lookup_symbol($3,VAR|PAR);
if(get_type(idx) != INTARRAY && get_type(idx) != UINTARRAY){
	err("%s is not an array nor a matrix, so length can't be used",$3);
}
int num = get_atr2(idx);
num = (get_atr3(idx) == 0) ? num : num*get_atr3(idx);
sprintf(str,"%d",num);
struct Exp_return vrati; vrati.index = insert_literal(strdup(str), INT);
 vrati.el_num=-1; $$ = &vrati;}
  ;

literal
  : _INT_NUMBER
      { $$ = insert_literal($1, INT); }

  | _UINT_NUMBER
      { $$ = insert_literal($1, UINT); }
  ;

function_call
  : _ID 
      {
        fcall_idx = lookup_symbol($1, FUN);
        if(fcall_idx == NO_INDEX)
          err("'%s' is not a function", $1);
      }
    _LPAREN argument _RPAREN
      {
        if(get_atr1(fcall_idx) != $4)
          err("wrong number of arguments");
        code("\n\t\t\tCALL\t%s", get_name(fcall_idx));
        if($4 > 0)
          code("\n\t\t\tADDS\t%%15,$%d,%%15", $4 * 4);
        set_type(FUN_REG, get_type(fcall_idx));
        $$ = FUN_REG;
      }
  ;

argument
  : /* empty */
    { $$ = 0; }

  | num_exp
    { 
      if(get_atr2(fcall_idx) != get_type($1))
        err("incompatible type for argument");
      if(get_type($1) == INTARRAY || get_type($1) == UINTARRAY){
	if((get_atr3($1)!=0 && get_atr3(fcall_idx)==0) || (get_atr3($1)==0 && get_atr3(fcall_idx)!=0) )
        err("incompatible type for argument");
      }
      free_if_reg($1);
      code("\n\t\t\tPUSH\t");
      gen_sym_name($1);
      $$ = 1;
    }
  ;

if_statement
  : if_part %prec ONLY_IF
      { code("\n@exit%d:", $1); }

  | if_part _ELSE statement
      { code("\n@exit%d:", $1); }
  ;

if_part
  : _IF _LPAREN
      {
        $<i>$ = ++lab_num;
        code("\n@if%d:", lab_num);
      }
    rel_exp
      {
        code("\n\t\t%s\t@false%d", opp_jumps[$4], $<i>3);
        code("\n@true%d:", $<i>3);
      }
    _RPAREN statement
      {
        code("\n\t\tJMP \t@exit%d", $<i>3);
        code("\n@false%d:", $<i>3);
        $$ = $<i>3;
      }
  ;

rel_exp
  : num_exp _RELOP num_exp
      {
        if(get_type($1) != get_type($3))
          err("invalid operands: relational operator");
        $$ = $2 + ((get_type($1) - 1) * RELOP_NUMBER);
        gen_cmp($1, $3);
      }
  ;

return_statement
  : _RETURN num_exp _SEMICOLON
      {
	int num_exp_type = get_type($2);
	num_exp_type = array_types[num_exp_type];
        if(get_type(fun_idx) != num_exp_type)
          err("incompatible types in return");
	int to_add = (part_of_array == 1) ? array_el_num : 0;
	gen_mov_add($2, FUN_REG,to_add,0);
        //gen_mov($2, FUN_REG);
	part_of_array = 0;
        code("\n\t\tJMP \t@%s_exit", get_name(fun_idx));        
      }
  ;

%%

int yyerror(char *s) {
  fprintf(stderr, "\nline %d: ERROR: %s", yylineno, s);
  error_count++;
  return 0;
}

void warning(char *s) {
  fprintf(stderr, "\nline %d: WARNING: %s", yylineno, s);
  warning_count++;
}

int main() {
  int synerr;
  init_symtab();
  output = fopen("output.asm", "w+");

  synerr = yyparse();

  clear_symtab();
  fclose(output);
  
  if(warning_count)
    printf("\n%d warning(s).\n", warning_count);

  if(error_count) {
    remove("output.asm");
    printf("\n%d error(s).\n", error_count);
  }

  if(synerr)
    return -1;  //syntax error
  else if(error_count)
    return error_count & 127; //semantic errors
  else if(warning_count)
    return (warning_count & 127) + 127; //warnings
  else
    return 0; //OK
}

