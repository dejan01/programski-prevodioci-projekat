//OPIS: operator :
//RETURN: 42

int main() {
  int a;
  int x;
  int niz[4,5] = {{11,2,3,4,6},{11,2,3,4,6},{11,2,3,4,6},{11,2,3,4,6}};
  niz[:,1] = 25;
  niz[3,:] = 5;
  a = niz[1,1];
  x = niz[3,1];
  niz[2,1] = 12;	
  return a + x + niz[2,1];
}



