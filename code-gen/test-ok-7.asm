
main:
		PUSH	%14
		MOV 	%15,%14
		MOV 	$11,-12(%14)
		MOV 	$2,-16(%14)
		MOV 	$3,-20(%14)
		MOV 	$4,-24(%14)
		SUBS	%15,$24,%15
@main_body:
		MOV 	$4,%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET